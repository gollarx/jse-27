package ru.t1.shipilov.tm.command.data;

import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.dto.Domain;
import ru.t1.shipilov.tm.enumerated.Role;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public final class DataBinaryLoadCommand extends AbstractDataCommand{

    @NotNull
    public static final String NAME = "data-load-bin";

    @NotNull
    public static final String DESCRIPTION = "Load data from binary file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BINARY LOAD]");
        @Cleanup @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @Cleanup @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public  String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
