package ru.t1.shipilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.enumerated.Status;
import ru.t1.shipilov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-change-status-by-id";

    @NotNull
    private final String DESCRIPTION = "Change project status by id.";

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.toStatus(statusValue);
        getProjectService().changeProjectStatusById(userId, id, status);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
